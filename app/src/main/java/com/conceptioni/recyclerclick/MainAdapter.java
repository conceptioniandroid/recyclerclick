package com.conceptioni.recyclerclick;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class MainAdapter extends RecyclerView.Adapter<MainAdapter.Holder> {
     private ArrayList<Model> matchesArrayList;

    public MainAdapter(ArrayList<Model> matchesArrayList) {
        this.matchesArrayList = matchesArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context mContext = parent.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new Holder(layoutInflater.inflate(R.layout.row_layout, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {

        holder.maintitle.setText(matchesArrayList.get(position).getMaintitle());
        holder.subtitle.setText(matchesArrayList.get(position).getSubtitle());

    }

    @Override
    public int getItemCount() {
        return matchesArrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView maintitle, subtitle;


        Holder(@NonNull View itemView) {
            super(itemView);

            maintitle = itemView.findViewById(R.id.main);
            subtitle = itemView.findViewById(R.id.sub);


        }
    }
}
