package com.conceptioni.recyclerclick;

public class Model {

    String maintitle, subtitle;

    public Model(String maintitle, String subtitle) {
        this.maintitle = maintitle;
        this.subtitle = subtitle;
    }

    public String getMaintitle() {
        return maintitle;
    }

    public void setMaintitle(String maintitle) {
        this.maintitle = maintitle;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
