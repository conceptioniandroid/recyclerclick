package com.conceptioni.recyclerclick;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    RecyclerView rec_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Model> modelArrayList = new ArrayList<>();
        modelArrayList.add(new Model("MainTitle1", "Subtitle1"));
        modelArrayList.add(new Model("MainTitle2", "Subtitle2"));
        modelArrayList.add(new Model("MainTitle3", "Subtitle3"));
        modelArrayList.add(new Model("MainTitle4", "Subtitle4"));
        modelArrayList.add(new Model("MainTitle5", "Subtitle5"));
        modelArrayList.add(new Model("MainTitle6", "Subtitle6"));
        modelArrayList.add(new Model("MainTitle7", "Subtitle7"));
        modelArrayList.add(new Model("MainTitle8", "Subtitle8"));
        modelArrayList.add(new Model("MainTitle11", "Subtitle11"));
        modelArrayList.add(new Model("MainTitle22", "Subtitle22"));
        modelArrayList.add(new Model("MainTitle33", "Subtitle33"));
        modelArrayList.add(new Model("MainTitle44", "Subtitle44"));
        modelArrayList.add(new Model("MainTitle55", "Subtitle55"));
        modelArrayList.add(new Model("MainTitle66", "Subtitle66"));
        modelArrayList.add(new Model("MainTitle77", "Subtitle77"));
        modelArrayList.add(new Model("MainTitle88", "Subtitle88"));

        rec_list = findViewById(R.id.rec);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        rec_list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        MainAdapter mAdapter1 = new MainAdapter(modelArrayList);
        rec_list.setAdapter(mAdapter1);

        rec_list.addOnItemTouchListener(new RecyclerTouchListener(this, rec_list, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                Toast.makeText(MainActivity.this, modelArrayList.get(position).getMaintitle() + " clicked", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onLongClick(View view, final int position) {

                Toast.makeText(MainActivity.this, modelArrayList.get(position).getMaintitle() + "  Long clicked", Toast.LENGTH_SHORT).show();
            }
        }));


    }
}
